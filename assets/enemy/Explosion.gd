extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite2D.play()
	$SoundExplosion.play()

func _on_sound_explosion_finished():
	queue_free()
