extends CanvasLayer

signal game_over

# Called when the node enters the scene tree for the first time.
func _ready():
	GLOBAL.score = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	$ScoreContainer/HBoxContainer/Score.text = str(GLOBAL.score)

func game_over_func():
	emit_signal("game_over")
	$GameOverContainer.visible = true
	$BgMusic.play()

func _on_restart_pressed():
	get_tree().reload_current_scene()
	#get_tree().call_deferred("reload_current_scene")

func _on_menu_pressed():
	get_tree().change_scene_to_file("res://assets/menu/Menu.tscn")
	#get_tree().call_deferred("change_scene", "res://assets/menu/Menu.tscn")

func _on_player_tree_exiting():
	game_over_func()
