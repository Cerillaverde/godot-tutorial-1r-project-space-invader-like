extends Area2D

@onready var player : CharacterBody2D = get_tree().get_nodes_in_group("player")[0]

const SPEED = 180

# Called when the node enters the scene tree for the first time.
func _ready():
	player.can_shoot = false
	$AnimatedSprite2D.play()
	$SoundShoot.play()

func _physics_process(delta):
	position.y -= SPEED * delta

func _on_area_entered(area):
	if area.is_in_group("enemy"):
		player.can_shoot = true
		queue_free()


func _on_visible_on_screen_notifier_2d_screen_exited():
	if is_instance_valid(player):
		player.can_shoot = true
	queue_free()
