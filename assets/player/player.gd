extends CharacterBody2D

@export var Shoot: PackedScene

const SPEED = 100.0
@onready var motion = Vector2.ZERO
@onready var can_shoot: bool = true
@onready var screensize = get_viewport_rect().size

func _ready():
	$AnimatedSprite2D.play()

func _physics_process(delta):
	motion_ctrl()
	animation_ctrl()
	motion = move_and_collide(motion * delta)
	

func _input(event):
	if event.is_action_pressed("ui_accept") and can_shoot:
		shoot_ctrl()

func get_axis() -> Vector2:
	var axis = Vector2.ZERO
	axis.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	axis.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	return axis

func motion_ctrl():
	if get_axis() == Vector2.ZERO:
		motion = Vector2.ZERO
	else:
		motion = get_axis().normalized() * SPEED
	
	#limitacion de movimiento
	position.x = clamp(position.x, 0, screensize.x)
	position.y = clamp(position.y, 0, screensize.y)

func animation_ctrl():
	if get_axis().x == 1:
		$AnimatedSprite2D.animation = "Horizontal"
		$AnimatedSprite2D.flip_h = true
	elif get_axis().x == -1:
		$AnimatedSprite2D.animation = "Horizontal"
		$AnimatedSprite2D.flip_h = false
	else:
		$AnimatedSprite2D.animation = "Vertical"

func shoot_ctrl():
	var shoot = Shoot.instantiate()
	shoot.global_position = $ShootSpawn.global_position
	get_tree().call_group("level", "add_child", shoot)




