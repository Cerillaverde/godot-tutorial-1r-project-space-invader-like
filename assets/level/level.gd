extends Node2D

@export var Enemy: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	$BgMusic.play()
	$EnemyTimer.start()
	randomize()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	get_node("Background").scroll_base_offset += Vector2(0, 1) * 8 * delta
	get_node("Clouds01").scroll_base_offset += Vector2(0, 1) * 24 * delta
	get_node("Clouds02").scroll_base_offset += Vector2(0, 1) * 34 * delta


func _on_hud_game_over():
	$BgMusic.stop()


func _on_enemy_timer_timeout():
	var patata = randi()%145
	get_node("EnemyPath/EnemySpawn").set_h_offset(patata)
	var enemy = Enemy.instantiate()
	add_child(enemy)
	enemy.position = get_node("EnemyPath/EnemySpawn").position
	$EnemyTimer.wait_time = GLOBAL.random(1, 3)
	$EnemyTimer.start()


